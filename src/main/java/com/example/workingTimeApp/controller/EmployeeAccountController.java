package com.example.workingTimeApp.controller;


import com.example.workingTimeApp.controller.api.request.AddEmployeeRequest;
import com.example.workingTimeApp.controller.api.response.AddEmployeeResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("employee-account")
public class EmployeeAccountController {

    @PostMapping(value = "/add", produces = "application/json")
    public ResponseEntity<AddEmployeeResponse> addEmployeeAccount(
            @RequestBody AddEmployeeRequest request
    ) {
        return null;
    }

//    @GetMapping(value = "/employee", produces = "application/json")
//    public ResponseEntity<Get<Employee>> getEmployeeData(
//            @PathVariable Long employeeId
//    ) {
//        return null;
//    }
}
