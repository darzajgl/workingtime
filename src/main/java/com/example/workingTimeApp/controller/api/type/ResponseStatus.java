package com.example.workingTimeApp.controller.api.type;

public enum ResponseStatus {
    SUCCESS,
    ERROR
}
