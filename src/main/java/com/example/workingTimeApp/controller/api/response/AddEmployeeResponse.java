package com.example.workingTimeApp.controller.api.response;

import com.example.workingTimeApp.controller.api.type.BasicResponse;

public class AddEmployeeResponse extends BasicResponse {
    private int employeeId;

    public AddEmployeeResponse(){}
    public AddEmployeeResponse(String responseMsg, int employeeId){
        super(responseMsg);
        this.employeeId = employeeId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
}
