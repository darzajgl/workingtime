package com.example.workingTimeApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.workingTimeApp.controller"})

public class workingTimeApplication {
    public static void main(String[] args) {
        SpringApplication.run(workingTimeApplication.class, args);


    }

}
